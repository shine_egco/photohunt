package com.example.photohunt;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.photohunt.database.*;


public class NormalLoginActivity extends Activity{

	
	
	//Normal login (not FB Login) Activity
	
	Database database; 
	EditText user_name_edt;
	EditText password_edt;
	Button submit_btn;
	String user_name_in;
	String password_in;
	
	
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_normal_login);
        ActionBar mActionBar = getActionBar();
        
        mActionBar.hide();
		database = new Database(this);
		user_name_edt = (EditText)findViewById(R.id.editText1);
		password_edt = (EditText)findViewById(R.id.editText2);
		submit_btn = (Button)findViewById(R.id.button1);
		submit_btn.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					
					user_name_in = user_name_edt.getText().toString();
					password_in = password_edt.getText().toString();
					
					if(user_name_in.equals("")||password_in.equals(""))
					{
						 Toast.makeText(NormalLoginActivity.this,"Please input any TextBox!!",
				       			 	Toast.LENGTH_LONG).show(); 
					
					}else if(user_name_in.length()<6)
					{
						 Toast.makeText(NormalLoginActivity.this,"Username must be at least 6 characters  ",
				       			 	Toast.LENGTH_LONG).show(); 
					}
					else if(password_in.length()<6)
					{

						 Toast.makeText(NormalLoginActivity.this,"Password must be at least 6 characters  ",
				       			 	Toast.LENGTH_LONG).show(); 
					}else
					{
						// Checking in case of Existing user account
						String exist_result =  database.CheckExistingUserAccount(user_name_in);
						if(exist_result.equals("exist"))
						{
								String authen_result = database.AuthenUserAccount(user_name_in, password_in);
								if(authen_result.equals("success"))
								{
								     Intent  start_main = new Intent(NormalLoginActivity.this,MainActivity.class);
								     		 start_main.putExtra("user_id",user_name_in);
								     		 start_main.putExtra("facebook_name",user_name_in);
								     		 start_main.putExtra("facebook_pic_url","http://i.imgur.com/AB7R6Hf.png");// Default Pic for no FB_Account User 
								     		 startActivity(start_main);
								     		 
									Toast.makeText(NormalLoginActivity.this,"Authen Success",
						       			 	Toast.LENGTH_LONG).show(); 
									
								}else
								{
									Toast.makeText(NormalLoginActivity.this,"Password mismatch ! ",
						       			 	Toast.LENGTH_LONG).show(); 
								}
								
						}
						else
						{
							 Toast.makeText(NormalLoginActivity.this,"Username or Password mismatch ! ",
					       			 	Toast.LENGTH_LONG).show(); 
						}
						
					}
					
					
				}
		 });
		
		
		
		
	}

}