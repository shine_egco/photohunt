package com.example.photohunt;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.ActionBar;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.photohunt.database.Database;
import com.example.photohunt.database.Photo;
import com.example.photohunt.database.UserAccount;
import com.example.photohunt.gps.GPSTracker;
import com.example.photohunt.tempsection.*;

public class PlayingActivity extends Activity  // call this activity by SectionOneFragment 
{
	String user_id ;
	String photo_clicked ;
	
	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static String TAG="PlayingAct";
	private Uri fileUri; // file url to store image/video
	private Uri fileUri2;
	final Context context = this;
	public Button capture_but ; 
	public static String path;
	double current_latitude;
	double current_longitude;
	protected GPSTracker gps;
	boolean flag= false;
	Dialog dialog;
	ImageView cap_photo_img_dialog;
	ImageView source_photo_img_dialog;
	ImageView source_photo_img;
	TextView similarity_rs ;
	TextView location_rs ;
	TextView final_rs;
	
	TextView image_id_tx;
	TextView image_des_tx;
	
	Button dialog_but;
	Database database;
	Photo source_photo;
	UserAccount user_acc;
	
	private static final String IMAGE_DIRECTORY_NAME = "PhotoHunt_Temp";
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playing);
        ActionBar mActionBar = getActionBar();
        mActionBar.hide();
        
        Intent from_section_one =  getIntent();
        Log.d(TAG,"before new database");
        database = new Database(this);
        Log.d(TAG,"After new database");
        
        user_id =  from_section_one.getStringExtra("user_id");
        photo_clicked  = from_section_one.getStringExtra("photo_clicked_id");
        
        user_acc = database.getUserAccount(user_id);
        
        Log.d(TAG,"photo_clicked is "+ photo_clicked);
        source_photo = database.getPhoto(photo_clicked);
        
      
        source_photo_img = (ImageView)findViewById(R.id.imageView1);
      
        image_id_tx = (TextView)findViewById(R.id.textView2);
        image_des_tx = (TextView)findViewById(R.id.textView4);
        image_id_tx.setText(""+source_photo.getPhotoID());
        image_des_tx.setText(source_photo.getDescript());
       
		BitmapFactory.Options options3 = new BitmapFactory.Options();
		options3.inSampleSize = 8;
		final Bitmap bitmap3 = BitmapFactory.decodeFile(source_photo.getPath(),options3);
		source_photo_img.setImageBitmap(bitmap3);

        
      //Dialog Zone
		dialog = new Dialog(context);
		
		dialog.setContentView(R.layout.picture_compare_dialog);
		dialog.setTitle("Compare");
		dialog_but  = (Button)dialog.findViewById(R.id.button1);
		cap_photo_img_dialog = (ImageView)dialog.findViewById(R.id.imageView1);
		source_photo_img_dialog = (ImageView)dialog.findViewById(R.id.ImageView2);
		similarity_rs = (TextView)dialog.findViewById(R.id.textView4);
		location_rs = (TextView)dialog.findViewById(R.id.textView2);
		final_rs = (TextView)dialog.findViewById(R.id.textView5);
		
		dialog_but.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				dialog.dismiss();
				finish();
			}
		});
		//End Dialog
        capture_but = (Button)findViewById(R.id.button1);
        
              
        capture_but.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// capture picture
				capture_but.setBackgroundColor(Color.rgb(73, 79, 93));
				try{
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					
					fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
		
					intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
			
					// start the image capture Intent
					startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
				
					
					}catch(Exception x)
					{
						Log.d(TAG,""+x);
					}
				// end capture Image 
			
				
			}
		}); // end capture_but.set.... 
        
    }

    @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// if the result is capturing Image
    	capture_but.setBackgroundColor(Color.rgb(57, 67, 89));
    	
		if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				// successfully captured the image
				// display it in image view
			
				gps = new GPSTracker(PlayingActivity.this);
				  if(gps.canGetLocation()){
			        	
			        	 current_latitude = gps.getLatitude();
			        	 current_longitude = gps.getLongitude();
			        	
			        	// \n is for new line
			        	Log.d(TAG,"current latitude is"+ current_latitude);
			        	Log.d(TAG,"current longtitude is :"+current_longitude);
			        }else{
			        	// can't get location
			        	// GPS or Network is not enabled
			        	// Ask user to enable GPS/network in settings
			        	gps.showSettingsAlert();
			        }
				  
					try{
						// Set Capture Photo to image view
						BitmapFactory.Options options = new BitmapFactory.Options();
						BitmapFactory.Options options2 = new BitmapFactory.Options();
						// downsizing image as it throws OutOfMemory Exception for larger
						// images
						options.inSampleSize = 8;
						final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
								options);
						cap_photo_img_dialog.setImageBitmap(bitmap);
						
						
						fileUri2 = Uri.parse(source_photo.getPath());
						Log.d(TAG,"source_photo.getPath ="+source_photo.getPath());
						options2.inSampleSize = 8;
						final Bitmap bitmap2 = BitmapFactory.decodeFile(fileUri2.getPath(),
								options2);
						
						source_photo_img_dialog.setImageBitmap(bitmap2);
						
						///////////////////////////////////////////////
						}catch(Exception x){Log.d(TAG,"Error occer at Bitmap "+x);}
					String result =  CheckingResult();
					if(result=="success")
					{
						int new_found_value=0;
						if(source_photo.getRating()==1)
						{
							new_found_value= user_acc.getFoundLvOne() +1 ;
							
						}else if (source_photo.getRating()==2)
						{
							new_found_value = user_acc.getFoundLvTwo() +1 ;
						}else if (source_photo.getRating()==3)
						{
							new_found_value = user_acc.getFoundLvThree() +1 ;
						}
						
						database.UpdateUserFinished(user_id,source_photo.getPhotoID(), new_found_value, source_photo.getRating());
						
						Toast.makeText(getApplicationContext(),
								"Update finished Value Success ", Toast.LENGTH_SHORT)
								.show();
						
					}else 
					{
					
					}
						
				
					dialog.getWindow().setLayout(1080,1850);
					dialog.show();

				
			} else if (resultCode == RESULT_CANCELED) {
				// user cancelled Image capture
				Toast.makeText(getApplicationContext(),
						"User cancelled image capture", Toast.LENGTH_SHORT)
						.show();
			} else {
				// failed to capture image
				Toast.makeText(getApplicationContext(),
						"Sorry! Failed to capture image", Toast.LENGTH_SHORT)
						.show();
			}
		} 
	}
    
    public Uri getOutputMediaFileUri(int type) {

		return Uri.fromFile(getOutputMediaFile(type));
	}

	/*
	 * returning image / video
	 */
	
	private static File getOutputMediaFile(int type) {
		
		Log.d(TAG,"getOutputMediaFile");
		// External sdcard location
		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(TAG, "Oops! Failed create "
						+ IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + ".jpg");
		
		} else {
			return null;
		}
		
		Log.d(TAG,""+mediaFile);
		
		path = mediaFile.toString();								
		return mediaFile;
	}
	
	public String CheckingResult (){
			
			Log.d(TAG,"Start CheckingResult");
			
			boolean flag_check_pos=false;
			boolean flag_check_photo = false;
			boolean check_photo = false;
			
			BitmapFactory.Options opt = new BitmapFactory.Options();
		    opt.inPreferredConfig = Bitmap.Config.RGB_565;
		    opt.inSampleSize = 5;
	
			Log.d(TAG,"1");
			Uri source_photo_uri = Uri.parse(source_photo.getPath());
			Log.d(TAG,"2"+ "source_photo.getPath is : "+source_photo.getPath());
			Uri cap_photo_uri = fileUri;
			Log.d(TAG,"3"+ "cap_photo_uri is : "+cap_photo_uri.toString());
			Bitmap source_bitmap = BitmapFactory.decodeFile(source_photo_uri.getPath(),opt);
			Log.d(TAG,"4");
			Bitmap cap_bitmap = BitmapFactory.decodeFile(cap_photo_uri.getPath(),opt);
			Log.d(TAG,"5");
			
			 int width1 = source_bitmap.getWidth();
			 int width2 = cap_bitmap.getWidth();
			 int height1 = source_bitmap.getHeight();
			 int height2 = cap_bitmap.getHeight();
			 
			 Log.d(TAG,"width1 :"+width1);
			 Log.d(TAG,"height1 :"+height1);
			 Log.d(TAG,"width2 :"+width2);
			 Log.d(TAG,"height2 :"+height2);
			 
			 
			 // Start check photo
			 if ((width1 != width2) || (height1 != height2)) { 
				 // check dimention of photo 
				 flag_check_pos = false;
				 flag_check_photo = false;
				 
				   	similarity_rs.setText("Failed");
			    	similarity_rs.setTextColor(Color.parseColor("#ca3c3c"));
			    	
				 Log.d(TAG,"Error!! Photo Dimention not Match!");
					Toast.makeText(getApplicationContext(),
							"Orientation Not Match Try Again", Toast.LENGTH_SHORT)
							.show();
				
			 }
			 else // 
			 {
				 Log.d(TAG,"Ok Photo Dimention Match!");
				 Color x ;
				 int num = 0;
				 long diff = 0;
				 
					    for (int i = 0; i < width1; i++) {
					      for (int j = 0; j < height1; j++) {
					    	
				    	  	int rgb1 = source_bitmap.getPixel(i, j);
					    	int rgb2 = cap_bitmap.getPixel(i, j);  
					    	
					    	// compare  Photo each pixel
					        int r1 = (rgb1 >> 16) & 0xff;
					        int g1 = (rgb1 >>  8) & 0xff;
					        int b1 = (rgb1      ) & 0xff;
					        int r2 = (rgb2 >> 16) & 0xff;
					        int g2 = (rgb2 >>  8) & 0xff;
					        int b2 = (rgb2      ) & 0xff;
					        					        
					        
					        diff += Math.abs(r1 - r2);
					        diff += Math.abs(g1 - g2);
					        diff += Math.abs(b1 - b2);
					        num  = num +1 ;
					        
					      // Log.d(TAG,"num is:"+num);
					        
					        
					      }
					    }
					    double n = width1 * height1 * 3;
					    double p = diff / n / 255.0;
					    double diff_percent = p*100;
					    double same_percent = 100-diff_percent;
					    Log.d(TAG,"Same Percent: " + same_percent);
					    
					    if(same_percent>80){
					    	flag_check_photo = true;
					    	similarity_rs.setText("Success");
					    	similarity_rs.setTextColor(Color.parseColor("#6eca3c"));
					    
					    }
					    else
					    {
					    	flag_check_photo = false;
					    	similarity_rs.setText("Failed");
					    	similarity_rs.setTextColor(Color.parseColor("#ca3c3c"));
					    	
					    }
					    
			 }// end check photo
			 
			 //Start check pos
			 
			 
			 Double source_photo_long = Double.parseDouble(source_photo.getLongtitude());
			 Double source_photo_lat = Double.parseDouble(source_photo.getLatitude());
			 
			 double diff_lat = 0 ;
			 double diff_long = 0;
			 double diff_sum=0;
			 diff_lat = source_photo_lat - current_latitude;
			 if(diff_lat<0)
			 {
				 diff_lat = diff_lat *-1;
			 }
			 
			 Log.d(TAG,"source_photo_lat:"+source_photo_lat);
			 Log.d(TAG,"current_latitude:"+current_latitude);
			 Log.d(TAG,"Diff lat :"+diff_lat);
			 
			 diff_long = source_photo_long - current_longitude;
			 if(diff_long<0)
			 {
				 diff_long = diff_long * -1;
			 }
					 
			 Log.d(TAG,"source_photo_long:"+source_photo_long);
			 Log.d(TAG,"current_longitude:"+current_longitude);
			 Log.d(TAG,"Diff long:"+ diff_long);
			 
			 diff_sum = (diff_lat+diff_long)*(Math.pow(10,5));
			 
			 //location_rs.setText("Diff sum :"+ diff_sum);

			 if(diff_sum<10)
			 {
				 flag_check_pos = true;
				 location_rs.setText("Success");
				 location_rs.setTextColor(Color.parseColor("#6eca3c"));
				 
				 
			 }else
			 {
				 flag_check_pos  = false;
				 location_rs.setText("Failed");
				 location_rs.setTextColor(Color.parseColor("#ca3c3c"));
			 }
			 
			 
			 
			 //end checl pos
			 
			if(flag_check_pos && flag_check_photo){	
				final_rs.setText("Success");
				final_rs.setTextColor(Color.parseColor("#6eca3c"));
				return "success";
			}
			else 
			{
				final_rs.setText("Failed");
				final_rs.setTextColor(Color.parseColor("#ca3c3c"));
				//similarity_rs.setText("Failed");
		    	//similarity_rs.setTextColor(Color.parseColor("#ca3c3c"));
		    	
				return "failed";	
			}
	} // end CheckResult
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}
	@Override
	public void onBackPressed() {
		finish();
	}

	
   
}