package com.example.photohunt.tempsection;
import java.util.ArrayList;
import java.util.List;

import com.example.photohunt.PlayingActivity;
import com.example.photohunt.R;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pkmmte.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.example.photohunt.database.*;


public class SectionOneFragmentTemp extends Fragment
{

	// this section represent User Profile and Playing Queue 
		
	TextView NameView ; 
	TextView InqueueView;
	TextView FinishedView;
	TextView NoPhotoCase;
	CircularImageView ProfileView;
	String user_id ;
	String facebook_name;
	String facebook_pic_url;
	Database database;
	UserAccount currentUser;
	
	
	protected GridView gridview;
	protected MyAdapter myAdapter;
	protected ArrayList<Photo> photo_queue;

	public String TAG = "SectionOne";
	
	// Adapter for Gridview 
	public class MyAdapter extends BaseAdapter
	{
		private LayoutInflater inflater;
	
		
		public MyAdapter (Context SectionOneFragmentTemp)
		{
			 inflater = LayoutInflater.from(SectionOneFragmentTemp);	 			
		}
		
		@Override
	    public int getCount() {
	   
			return photo_queue.size();	
	    }
	
	    @Override
	    public Object getItem(int i)
	    {
	        return photo_queue.get(i);
	    	
	    }
	
	    @Override
	    public long getItemId(int i)
	    {
	       
	    	return i;
	    }
	
		@Override
		public View getView (int i , View view , ViewGroup viewGroup)
		{
			//Managing each items in gridview (Link to gridview_section_one.xml)
		
			View v = view;
			TextView image_id ;
			TextView status ; 
			RatingBar rating_bar;
			ImageView photo_img;
			
			
			//initial item in gridview
			if(v == null)
            {
				v = inflater.inflate(R.layout.gridview_section_one, viewGroup, false);
				v.setTag(R.id.textView2, v.findViewById(R.id.textView2));
				v.setTag(R.id.TextView01, v.findViewById(R.id.TextView01));
				v.setTag(R.id.ratingBar1,v.findViewById(R.id.ratingBar1));
				v.setTag(R.id.imageView1,v.findViewById(R.id.imageView1));
			 
            }
			
			image_id = (TextView)v.getTag(R.id.textView2);
            status = (TextView)v.getTag(R.id.TextView01);
            rating_bar = (RatingBar)v.getTag(R.id.ratingBar1);
            photo_img = (ImageView)v.getTag(R.id.imageView1);            
                       
            image_id.setText(""+photo_queue.get(i).getPhotoID());
            status.setText("Finding"); 
            rating_bar.setRating(photo_queue.get(i).getRating());
         
                    
         
          	 
   			 Uri path_uri = Uri.parse(photo_queue.get(i).getPath());
   			 Log.d(TAG,"path uri is:"+path_uri);
   			
			BitmapFactory.Options options = new BitmapFactory.Options();
			// downsizing image as it throws OutOfMemory Exception for larger
			// images
			
			Uri fileUri = Uri.parse(photo_queue.get(i).getPath());
			
			options.inSampleSize = 8;
			final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
					options);
           
        
			photo_img.setImageBitmap(bitmap);
			
	        return v;
            
	}//end Constructor MyAdapter
	
}// end Class MyAdapter
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) { 

		View rootView = inflater.inflate(R.layout.fragment_section_one, container, false);
		
		
		user_id = getArguments().getString("user_id");
		facebook_name = getArguments().getString("facebook_name");
		facebook_pic_url = getArguments().getString("facebook_pic_url");
		
		database = new Database(this.getActivity());
		
		photo_queue = database.getPlayingQueue(user_id);
		
		NoPhotoCase = (TextView)rootView.findViewById(R.id.textView3);
		NameView  = (TextView)rootView.findViewById(R.id.textView1);
		InqueueView = (TextView)rootView.findViewById(R.id.textView4);
		FinishedView = (TextView)rootView.findViewById(R.id.TextView02);
		ProfileView = (CircularImageView)rootView.findViewById(R.id.imageView1);
		currentUser = database.getUserAccount(user_id);
		NameView.setText(currentUser.getFacebookName());
		InqueueView.setText(photo_queue.size()+" "+"items");
		
		int finish_num = currentUser.getFoundLvOne() + currentUser.getFoundLvTwo() + currentUser.getFoundLvThree() ; 
		
		FinishedView.setText(""+finish_num);
		
		// set Profile Picture
		Picasso.with(this.getActivity()).load(facebook_pic_url).into(ProfileView);		
		
		if(photo_queue.size()!=0){
			
			NoPhotoCase.setText("");
		}
		
		
		Log.d(TAG,"photo_queue size is :"+ photo_queue.size()+"");
		
		
		gridview =(GridView)rootView.findViewById(R.id.gridView1);
		myAdapter = new MyAdapter(rootView.getContext());
		gridview.setAdapter(myAdapter);

	
		gridview.setOnItemClickListener(new OnItemClickListener() 
		{
		

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				Intent toPlayAct = new Intent(getActivity(),PlayingActivity.class);
				Photo temp_photo =  (Photo) myAdapter.getItem(position);
							
				Log.d(TAG,"temp_photo is:"+temp_photo.getPhotoID());

			
			
				toPlayAct.putExtra("user_id",user_id);
				toPlayAct.putExtra("photo_clicked_id",Integer.toString(temp_photo.getPhotoID()));
				
				
				
				startActivity(toPlayAct);		
				getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
				
			}
			
		});  
		
		return rootView;
		
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		// Managing Gridview in case for Database changed 
		currentUser = database.getUserAccount(user_id);
		ProfileView.setBackgroundColor(Color.TRANSPARENT);
		
		
		
		Log.d(TAG,"getFoundLv3:" + currentUser.getFoundLvThree());
		int finish_num = currentUser.getFoundLvOne() + currentUser.getFoundLvTwo() + currentUser.getFoundLvThree() ; 
		FinishedView.setText(""+finish_num+"items "+"("+currentUser.getFoundLvOne()+" / "+
													    currentUser.getFoundLvTwo()+" / "+
														currentUser.getFoundLvThree()+")");
		
		photo_queue = database.getPlayingQueue(user_id);
		InqueueView.setText(photo_queue.size()+" "+"items");
		myAdapter.notifyDataSetChanged();
		gridview.invalidateViews();
		gridview.setAdapter(myAdapter);
		
	}
	
	
}