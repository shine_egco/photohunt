package com.example.photohunt.tempsection;
import java.util.ArrayList;
import java.util.List;

import com.example.photohunt.AdminActivity;
import com.example.photohunt.R;

import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.photohunt.database.*;
import com.example.photohunt.tempsection.SectionOneFragmentTemp.MyAdapter;

public class SectionTwoFragmentTemp extends Fragment
{
	
	private static final String TAG = "SectionTwo";
	protected GridView gridview;
	protected MyAdapter myAdapter;
	protected ArrayList<Photo> allPhoto_list ;
	protected List<Photo> photo_choose = new ArrayList<Photo>();
	protected Database database;
	private Context context;
	private String user_id;
	private int photo_id_clicked=0;
	Photo source_photo;
	ImageView source_photo_img;
	TextView NoPhotoCase;

	
	
	// Adapter for Gridview (all Photo list )
	public class MyAdapter extends BaseAdapter
	{
		private LayoutInflater inflater;
	
		
		public MyAdapter (Context SectionTwoFragmentTemp)
		{
			 inflater = LayoutInflater.from(SectionTwoFragmentTemp);	 			
		}
		
		@Override
	    public int getCount() {
	    
			
			return allPhoto_list.size();
	    }
	
	    @Override
	    public Object getItem(int i)
	    {
	        return allPhoto_list.get(i);
	    }
	
	    @Override
	    public long getItemId(int i)
	    {
	    	return i;
	    }
	
		@Override
		public View getView (int i , View view , ViewGroup viewGroup)
		{
			// Managing each item in gridview (link to grid_view_section_two.xml)
			
			
			View v = view;
			TextView image_id;
			RatingBar  rating_bar;
			ImageView image_view ;
		
			// initial in gridview
			if(v == null)
            {
				v = inflater.inflate(R.layout.gridview_section_two, viewGroup, false);
				v.setTag(R.id.textView2, v.findViewById(R.id.textView2));
				v.setTag(R.id.ratingBar1,v.findViewById(R.id.ratingBar1));
				v.setTag(R.id.imageView1,v.findViewById(R.id.imageView1));
            }
		
			 image_id = (TextView)v.getTag(R.id.textView2);
			 rating_bar = (RatingBar)v.getTag(R.id.ratingBar1);
			 rating_bar.setFocusable(false);
			 rating_bar.cancelPendingInputEvents();
			 
			 image_view = (ImageView)v.getTag(R.id.imageView1);
			 image_id.setText(""+allPhoto_list.get(i).getPhotoID());
			 rating_bar.setRating(allPhoto_list.get(i).getRating());
			 
			 Uri path_uri = Uri.parse(allPhoto_list.get(i).getPath());
			 Log.d(TAG,"path uri is:"+path_uri);
			
				BitmapFactory.Options options = new BitmapFactory.Options();
				// downsizing image as it throws OutOfMemory Exception for larger
				// images
				
				Uri fileUri = Uri.parse(allPhoto_list.get(i).getPath());
				
				options.inSampleSize = 8;
				final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
						options);
			
			 image_view.setImageBitmap(bitmap);				

					
	        return v;
            
	}//end Constructor MyAdapter
	
}// end Class MyAdapter
	
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) { 

		View rootView = inflater.inflate(R.layout.fragment_section_two, container, false);
		user_id = getArguments().getString("user_id");
		
		context = getActivity();
		database = new Database(this.getActivity());
		allPhoto_list = database.getAllPhoto();
		Log.d(TAG,"allPhoto_list size is :"+allPhoto_list.size());
		
		NoPhotoCase = (TextView)rootView.findViewById(R.id.textView1);
		
		if(allPhoto_list.size()!=0){
			NoPhotoCase.setText("");
		}
		
		
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.show_detail_dialog);
		final Button add_queue_but  = (Button)dialog.findViewById(R.id.button1);
		source_photo_img = (ImageView)dialog.findViewById(R.id.imageView1);
	
		add_queue_but.setOnClickListener(new OnClickListener() {
		
				@Override
				
				public void onClick(View v) {

					add_queue_but.setBackgroundColor(Color.rgb(32, 73, 116));
				//	String exist_history = database
					
					String exist_history = database.CheckExistingHistory(user_id,Integer.toString(photo_id_clicked));
					if(exist_history.equals("exist"))
					{
						Toast.makeText(getActivity(),"Sorry this Photo is in Queue or successed ",Toast.LENGTH_LONG).show();
					}
					else{
						
						Log.d(TAG,"Prepareing to insert History");
						Log.d(TAG,"user_id is:"+user_id);
						long flg1 = database.InsertHistory(user_id,photo_id_clicked,"finding");
						if(flg1 > 0)
			          	{
			          	 Toast.makeText(getActivity(),"Insert(1) Data Successfully",
			          			 	Toast.LENGTH_LONG).show(); 
			          	}
			          	else
			          	{
			             	 Toast.makeText(getActivity(),"Insert(1) Data Failed.",
			       			 	Toast.LENGTH_LONG).show(); 
			          	}
	          	
					}
					
					dialog.dismiss();
				}
				
		});
		
		Log.d(TAG,"8");
		final Button cancel_but = (Button)dialog.findViewById(R.id.button2);
		cancel_but.setOnClickListener(new OnClickListener() {
		
				@Override
				
				public void onClick(View v) {
					cancel_but.setBackgroundColor(Color.rgb(120,21,21));
				
					dialog.dismiss();
				
				}
				
		});
		Log.d(TAG,"9");
	
		
		gridview =(GridView)rootView.findViewById(R.id.gridView1);
		myAdapter = new MyAdapter(rootView.getContext());
		gridview.setAdapter(myAdapter);
		
		gridview.setOnItemClickListener(new OnItemClickListener() 
		{
			
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				Log.d(TAG,"gridview item clicked detect");
				// TODO Auto-generated method stub	
					TextView photo_id_TV = (TextView)view.findViewById(R.id.textView2);
					photo_id_clicked =  Integer.parseInt(photo_id_TV.getText().toString());
					Log.d(TAG,"photo_id_clicked is:"+photo_id_clicked);
					source_photo = database.getPhoto(photo_id_TV.getText().toString());
					
					
					Uri fileUri = Uri.parse(source_photo.getPath());
					BitmapFactory.Options options = new BitmapFactory.Options();
					options.inSampleSize = 8;
					final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),options);
					source_photo_img.setImageBitmap(bitmap);
			    		  
					cancel_but.setBackgroundColor(Color.rgb(189,33,33));
					add_queue_but.setBackgroundColor(Color.rgb(51, 115, 166));
					dialog.show();
				
			}
			
		});
		return rootView;
		
	}
	
}