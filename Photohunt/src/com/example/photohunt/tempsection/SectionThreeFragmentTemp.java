package com.example.photohunt.tempsection;
import java.util.ArrayList;

import java.util.List;

import com.example.photohunt.R;

import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.example.photohunt.database.*;
import com.example.photohunt.tempsection.SectionOneFragmentTemp.MyAdapter;


public class SectionThreeFragmentTemp extends Fragment
{
	
	// this section represent user success photo 
	
	
	Database database ; 
	String user_id ; 
	TextView NoPhotoCase;
	protected ArrayList<Photo> successPhoto_list ;
	public String TAG = "SectionThree";
	protected GridView gridview;
	protected MyAdapter myAdapter;
	ImageView source_photo_img;
	private int photo_id_clicked=0;
	Photo source_photo;

	public class MyAdapter extends BaseAdapter
	{
		private LayoutInflater inflater;
	
		
		public MyAdapter (Context SectionTwoFragmentTemp)
		{
			 inflater = LayoutInflater.from(SectionTwoFragmentTemp);	 			
		}
		
		@Override
	    public int getCount() {
	    			
			return successPhoto_list.size();
	    }
	
	    @Override
	    public Object getItem(int i)
	    {
	        return successPhoto_list.get(i);
	    }
	
	    @Override
	    public long getItemId(int i)
	    {
	  
	    	return i;
	    }
	
		@Override
		public View getView (int i , View view , ViewGroup viewGroup)
		{
		// Managing each item in gridview (link to grid_view_section_three.xml)
			
			
			View v = view;
			TextView image_id ;
			TextView status ; 
			RatingBar rating_bar;
			ImageView photo_img;
			
			// initial in gridview
			if(v == null)
            {
				v = inflater.inflate(R.layout.gridview_section_three, viewGroup, false);
				v.setTag(R.id.textView2, v.findViewById(R.id.textView2));
				v.setTag(R.id.TextView01, v.findViewById(R.id.TextView01));
				v.setTag(R.id.ratingBar1,v.findViewById(R.id.ratingBar1));
				v.setTag(R.id.imageView1,v.findViewById(R.id.imageView1));
		
            }
			image_id = (TextView)v.getTag(R.id.textView2);
            status = (TextView)v.getTag(R.id.TextView01);
            rating_bar = (RatingBar)v.getTag(R.id.ratingBar1);
            photo_img = (ImageView)v.getTag(R.id.imageView1);            
            image_id.setText(""+successPhoto_list.get(i).getPhotoID());
            status.setText("Success"); // waiting for implement
            rating_bar.setRating(successPhoto_list.get(i).getRating());
            
            Uri path_uri = Uri.parse(successPhoto_list.get(i).getPath());
  			 Log.d(TAG,"path uri is:"+path_uri);
  			
			BitmapFactory.Options options = new BitmapFactory.Options();
			// downsizing image as it throws OutOfMemory Exception for larger
			// images
			
			Uri fileUri = Uri.parse(successPhoto_list.get(i).getPath());
			
			options.inSampleSize = 8;
			final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
					options);
          
			photo_img.setImageBitmap(bitmap);
         
					
	        return v;
            
	}//end Constructor MyAdapter
	
}// end Class MyAdapter
	
	
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) { 

		View rootView = inflater.inflate(R.layout.fragment_section_three, container, false);
		
		database = new Database(this.getActivity());
		user_id = getArguments().getString("user_id");
		successPhoto_list = database.getSuccessPhoto(user_id);
		
		NoPhotoCase = (TextView)rootView.findViewById(R.id.textView1);
		
		if(successPhoto_list.size()!=0){
			NoPhotoCase.setText("");
		}
		
		final Dialog dialog = new Dialog(this.getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.show_detail_dialog_2);
		source_photo_img = (ImageView)dialog.findViewById(R.id.imageView1);
		
		
		
		
		gridview =(GridView)rootView.findViewById(R.id.gridView1);
		myAdapter = new MyAdapter(rootView.getContext());
		gridview.setAdapter(myAdapter);
		
		gridview.setOnItemClickListener(new OnItemClickListener() 
		{
			
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				TextView photo_id_TV = (TextView)view.findViewById(R.id.textView2);
				photo_id_clicked =  Integer.parseInt(photo_id_TV.getText().toString());
				source_photo = database.getPhoto(photo_id_TV.getText().toString());
				
				Uri fileUri = Uri.parse(source_photo.getPath());
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inSampleSize = 8;
				final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),options);
				source_photo_img.setImageBitmap(bitmap);
				dialog.show();
					
				
			}
			
		});
		
		return rootView;
		
	}
	
}