package com.example.photohunt;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.photohunt.database.*;

public class RegisterActivity extends Activity{
	
	
	
	//Register Activity for Create User Account to Database
	
	Button submit_but;
	EditText user_name_edt;
	EditText password_edt;
	EditText c_password_edt;
	
	String user_name;
	String password;
	String c_password;
	
	Database database;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
	   
		database = new Database(this);
        ActionBar mActionBar = getActionBar();
        mActionBar.hide();
        
		submit_but = (Button) findViewById(R.id.button1);
		user_name_edt = (EditText)findViewById(R.id.editText1);
		password_edt = (EditText)findViewById(R.id.editText2);
		c_password_edt = (EditText)findViewById(R.id.editText3);
		
	     submit_but.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {

					user_name = user_name_edt.getText().toString();
					password = password_edt.getText().toString();
					c_password = c_password_edt.getText().toString();
					
					
					if(user_name.equals("")||password.equals("")||password.equals(""))
					{
						 Toast.makeText(RegisterActivity.this,"Please input any TextBox!!",
				       			 	Toast.LENGTH_LONG).show(); 
						
					}else if (!password.equals(c_password))
					{
						 Toast.makeText(RegisterActivity.this,"Password Not Matching!! ",
				       			 	Toast.LENGTH_LONG).show(); 
					}
					else if(user_name.length()<6)
					{
						 Toast.makeText(RegisterActivity.this,"Username must be at least 6 characters  ",
				       			 	Toast.LENGTH_LONG).show(); 
					}
					else if(password.length()<6)
					{

						 Toast.makeText(RegisterActivity.this,"Password must be at least 6 characters  ",
				       			 	Toast.LENGTH_LONG).show(); 
					}
					else
					{
						//Checking Existing Account
						String result =  database.CheckExistingUserAccount(user_name);
						if(result.equals("not_exist")){
							// Add Database
							 database.InsertUser(user_name, user_name,password, 0, 0, 0);
							
							 Toast.makeText(RegisterActivity.this,"Register Success",
					       			 	Toast.LENGTH_LONG).show(); 
							 finish();
							
						}else{
							 
							 Toast.makeText(RegisterActivity.this,"Sorry T-T,Someone already has that username.",
					       			 	Toast.LENGTH_LONG).show(); 
						}
					}
					
				}
		    	  
		      });
		
		
	}
	
	
	
}