package com.example.photohunt;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


import com.example.photohunt.gps.*;
import com.example.photohunt.database.*;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;
import android.widget.VideoView;



public class AdminActivity extends Activity{
	
	
		// Activity for Add Photo to Database 
	
		Button addPhoto_but;
		Button submit_but;
		EditText descript_edt;
		Database database;
		String descript_str;
		public static String path;
		double latitude;
		double longitude;
		RatingBar rating_bar;
		int rating;
		
		// Activity request codes
		private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
		public static final int MEDIA_TYPE_IMAGE = 1;
		public static final String TAG = "admin_act";
		// directory name to store captured images and videos
		private static final String IMAGE_DIRECTORY_NAME = "PhotoHunt_Image";

		protected GPSTracker gps;
		private Uri fileUri; // file url to store image/video

		
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_admin);
	
		
		  database = new Database(this);
		  database.getWritableDatabase(); // First method
		  rating_bar = (RatingBar)findViewById(R.id.ratingBar1);
		  
		  addPhoto_but = (Button)findViewById(R.id.button1);
	      addPhoto_but.setOnClickListener(new OnClickListener(){
	    	 
				@Override
				public void onClick(View v) {
					
					try{
						Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
						
						fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
			
						intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
				
						
						// start the image capture Intent
						startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
						
						}catch(Exception x)
						{
							Log.d(TAG,""+x);
						}
		
				}
		    	  
		      });
		
	      descript_edt  = (EditText)findViewById(R.id.editText1);
	      
	      submit_but = (Button)findViewById(R.id.button2);
	      submit_but.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					
					//Button for add photo to database
		
					descript_str = descript_edt.getText().toString();
					rating=  (int) rating_bar.getRating();
					
					
					Log.d(TAG,"Prepare InsertPhotoDB");
					Log.d(TAG,"latitiude is:"+latitude);
					Log.d(TAG,"Rating is "+rating);
					Log.d(TAG,"longitude is:"+longitude);
					Log.d(TAG,"descript is:"+descript_str);
					Log.d(TAG,"path is"+path);
					
					
					
						long flg1 = database.InsertPhoto(latitude,longitude,rating,descript_str,path);
						if(flg1 > 0)
			          	{
			          	 Toast.makeText(AdminActivity.this,"Insert(1) Data Successfully",
			          			 	Toast.LENGTH_LONG).show(); 
			          	}
			          	else
			          	{
			             	 Toast.makeText(AdminActivity.this,"Insert(1) Data Failed.",
			       			 	Toast.LENGTH_LONG).show(); 
			          	}
		          						
					
				}
		    	  
		      });
	      
	      
	      
	      
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// if the result is capturing Image
		if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				// successfully captured the image
				// display it in image view
					
				//get Gps Postition of Current photo 
				gps = new GPSTracker(AdminActivity.this);
				  if(gps.canGetLocation()){
			        	
			        	 latitude = gps.getLatitude();
			        	 longitude = gps.getLongitude();
			        	
			        	// \n is for new line
			        	Log.d(TAG,"latitude is"+ latitude);
			        	Log.d(TAG,"longtitude is :"+longitude);
			        }else{
			        	// can't get location
			        	// GPS or Network is not enabled
			        	// Ask user to enable GPS/network in settings
			        	gps.showSettingsAlert();
			        }
				  
				
				
			} else if (resultCode == RESULT_CANCELED) {
				// user cancelled Image capture
				Toast.makeText(getApplicationContext(),
						"User cancelled image capture", Toast.LENGTH_SHORT)
						.show();
			} else {
				// failed to capture image
				Toast.makeText(getApplicationContext(),
						"Sorry! Failed to capture image", Toast.LENGTH_SHORT)
						.show();
			}
		} 
	}

	public Uri getOutputMediaFileUri(int type) {
		Log.d(TAG,"getOutMediaFileUri");
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/*
	 * returning image 
	 */
	
	private static File getOutputMediaFile(int type) {
		
		Log.d(TAG,"getOutputMediaFile");
		// External sdcard location
		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(TAG, "Oops! Failed create "
						+ IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + ".jpg");
		} else {
			return null;
		}
		
		Log.d(TAG,""+mediaFile);
		
		path = mediaFile.toString();
		
		return mediaFile;
	}

	
}