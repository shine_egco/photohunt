package com.example.photohunt;


import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;  

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.example.photohunt.database.Database;
import com.facebook.SessionDefaultAudience;
//import com.squareup.picasso.Picasso;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.Permission.Type;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.entities.Profile.Properties;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnLogoutListener;
import com.sromku.simple.fb.listeners.OnProfileListener;
import com.sromku.simple.fb.utils.Attributes;
import com.sromku.simple.fb.utils.Logger;
import com.sromku.simple.fb.utils.PictureAttributes;
import com.sromku.simple.fb.utils.PictureAttributes.PictureType;

import android.graphics.Typeface;

public class LoginActivity extends FragmentActivity 
{	
	private SimpleFacebook mSimpleFacebook;
	private static final String APP_ID = "287791158072780";
	private static final String APP_NAMESPACE = "photohunt_ns";
	public String TAG = "LogAct";

	Intent start_main ;
	public Database database;

	ImageView fb_login_but ; 
	Button register_but;
	Button login_but;
	Button logout_but;
	Button testDB_but;
	Button admin_but;
	ImageView profile_img ;
	
	public String facebook_id="";
	public String facebook_name="";
	public String facebook_pic_url = "";

	private Context context;
	

	 protected void onCreate(Bundle savedInstanceState) {
	      super.onCreate(savedInstanceState);
	      this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	      setContentView(R.layout.activity_login);
	     
	      database = new Database(this);

		  database.getWritableDatabase(); 
	      
	      start_main = new Intent(LoginActivity.this,MainActivity.class);
	      
	      context = this;
			Logger.DEBUG_WITH_STACKTRACE = true;

			// initialize facebook configuration
			Permission[] permissions = new Permission[] { 
					Permission.PUBLIC_PROFILE, 
					Permission.USER_GROUPS,
					Permission.USER_BIRTHDAY, 
					Permission.USER_LIKES, 
					Permission.USER_PHOTOS,
					Permission.USER_VIDEOS,
					Permission.USER_FRIENDS,
					Permission.USER_EVENTS,
					Permission.USER_VIDEOS,
					Permission.USER_RELATIONSHIPS,
					Permission.READ_STREAM, 
					Permission.PUBLISH_ACTION
					};

			SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
				.setAppId(APP_ID)
				.setNamespace(APP_NAMESPACE)
				.setPermissions(permissions)
				.build();

			SimpleFacebook.setConfiguration(configuration);
			
			mSimpleFacebook = SimpleFacebook.getInstance(this);
	     
			//////////////////Login Button ///////////////////////
	      fb_login_but = (ImageView)findViewById(R.id.imageView2);
	      fb_login_but.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				 mSimpleFacebook.login(new OnLoginListener(){

						@Override
						public void onThinking() {
							// TODO Auto-generated method stub
							Toast.makeText(context,"Logging in", Toast.LENGTH_SHORT).show();
						}

						@Override
						public void onException(Throwable throwable) {
							// TODO Auto-generated method stub
							Toast.makeText(context,throwable.toString(), Toast.LENGTH_SHORT).show();
							
						}

						@Override
						public void onFail(String reason) {
							// TODO Auto-generated method stub
							Toast.makeText(context,"Fail..", Toast.LENGTH_SHORT).show();
							
						}

						@Override
						public void onLogin() {
							// TODO Auto-generated method stub
								Toast.makeText(context,"Login Successful", Toast.LENGTH_SHORT).show();
								
								GetFbData();
																
								Log.d(TAG,"onLogin() facebook id :"+facebook_id);
								Log.d(TAG,"onLogin() facebook name :"+facebook_name);
										
															
						}

						@Override
						public void onNotAcceptingPermissions(Type type) {
							// TODO Auto-generated method stub
							Toast.makeText(context,"Not Accept Permission", Toast.LENGTH_SHORT).show();
							
						}
				    	  
				    	  
				      });				
			}
	    	  
	    	
			
	      }
	      
	    		  
	    		  );
	      
	  /////////////////////////////////////////////////////
	      
	  ///////////////////Logout Button/////////////////////
	      logout_but = (Button)findViewById(R.id.button2);
	      logout_but.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mSimpleFacebook.logout(new OnLogoutListener(){

					@Override
					public void onThinking() {
						
						Toast.makeText(context,"Logout", Toast.LENGTH_SHORT).show();						
					}

					@Override
					public void onException(Throwable throwable) {
						// TODO Auto-generated method stub
						Toast.makeText(context,throwable.toString(), Toast.LENGTH_SHORT).show();
						
					}

					@Override
					public void onFail(String reason) {
						// TODO Auto-generated method stub
						Toast.makeText(context,reason, Toast.LENGTH_SHORT).show();
						
					}

					@Override
					public void onLogout() {
						// TODO Auto-generated method stub
						Toast.makeText(context,"Logout Sucessful".toString(), Toast.LENGTH_SHORT).show();
					}
					
					
				});
				
			}
	    	  
	      });
	     	      
	      // Admin Button 
	      
	      admin_but = (Button)findViewById(R.id.button3);
	      admin_but.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					//Start Admin Activity	
					Intent start_admin = new Intent(LoginActivity.this,AdminActivity.class);
					startActivity(start_admin);
				}
		    	  
		      });

	      
	      /////////////////////////////////////////////////////
	      
	      // Register Button
	      
	      register_but = (Button)findViewById(R.id.button4);
	      register_but.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					Intent start_register = new Intent(LoginActivity.this,RegisterActivity.class);
					startActivity(start_register);
				}
		    	  
		      });
	      
	      /////////////////////////////////////////////////////
	      
	      //Login Button
	     login_but = (Button)findViewById(R.id.button5);
	     login_but.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					Intent start_login = new Intent(LoginActivity.this,NormalLoginActivity.class);
					startActivity(start_login);
				}
		    	  
		      });
	     
	     //////////////////////////////////////////////////////
	      
	      
	 }// end OnCreate


		@Override
		 protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		     mSimpleFacebook.onActivityResult(this, requestCode, resultCode, data); 
		     super.onActivityResult(requestCode, resultCode, data);
		 } 
		@Override
		protected void onResume() {
			super.onResume();
			mSimpleFacebook = SimpleFacebook.getInstance(this);
			//Toast.makeText(context,mSimpleFacebook.isLogin()+"", Toast.LENGTH_SHORT).show();
		}
		public void GetFbData()
		{
			//Specifies Profile Photo From Facebook 
			PictureAttributes pictureAttributes = Attributes.createPictureAttributes();
			pictureAttributes.setHeight(250);
			pictureAttributes.setWidth(250);
			pictureAttributes.setType(PictureType.SQUARE);
			
			
			Profile.Properties properties = new Profile.Properties.Builder()
		    .add(Properties.ID)
		    .add(Properties.NAME)
		    .add(Properties.PICTURE,pictureAttributes)
		    .build();
			
			
			OnProfileListener onProfileListener = new OnProfileListener() {         
			    @Override
			    public void onComplete(Profile profile) {
			        Log.d(TAG, "My profile id = " + profile.getId());
			        Log.d(TAG,"Profile Name is :"+ profile.getName());
			        Log.d(TAG,"Photo String is:"+ profile.getPicture());
			        facebook_id = profile.getId().toString();
			        facebook_name = profile.getName().toString();
			        facebook_pic_url = profile.getPicture().toString();
			        
			        
			        System.out.println("facebook_id:"+facebook_id);
			        System.out.println("facebook_name:"+facebook_name);
			        System.out.println("facebook_pic:"+facebook_pic_url);
			        
			        
			        	start_main.putExtra("user_id",facebook_id);
						start_main.putExtra("facebook_name",facebook_name);
						start_main.putExtra("facebook_pic_url",facebook_pic_url);
						
						String check_user_exist = database.CheckExistingUserAccount(facebook_id);
						Log.d(TAG,"check_user_exist is:"+check_user_exist);
						if(check_user_exist=="exist")
						{
							Log.d(TAG,"User Account Exist");
						}
						else {
							try{
									// Create UserAccount by Facebook Login
									database.InsertUser(facebook_id,facebook_name,"NoFbPassword",0, 0, 0);
							}catch(Exception x){Log.d(TAG,"Error occer can't InsertUser"+x);}
							
						}
			        
						startActivity(start_main);
						
			
			        
						
			    }

			};
			

			mSimpleFacebook.getProfile(properties,onProfileListener);
			
			
			
			Log.d(TAG,"Fininsh GetFBData()");
			

		}



}