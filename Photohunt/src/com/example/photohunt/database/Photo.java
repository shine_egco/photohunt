package com.example.photohunt.database;

import java.util.Date;


public class Photo{
	
	private int photo_id;
	private String longtitude;
	private String latitude;
	private int rating; //  difficulty  level of photo
	private String descript; // hint description of photo
	private String path; // directory path of photo in device 
	
	
	public Photo(int id_photo_in 
			,String longtitude_in
			,String latitude_in
			,int rating_in
			,String descript_in
			,String path_in)
	{
		this.photo_id = id_photo_in;
		this.latitude = latitude_in;
		this.longtitude = longtitude_in;
		this.descript = descript_in;
		this.path = path_in;
		this.rating  = rating_in;
		
	}
	public int getPhotoID (){return photo_id;}
	public String getLongtitude() {return longtitude;}
	public String getLatitude(){return latitude;}
	public String getDescript(){return descript;}
	public String getPath(){return path;}
	public int getRating(){return rating;}
	
	
}

