package com.example.photohunt.database;


public class History{
	
	private String history_id;
	private String user_id;
	private String status; 
	
	
	public History(String history_id_in , String user_id_in , String status_in)
	{
		history_id = history_id_in;
		user_id = user_id_in;
		status = status_in;
		
	}
	public String getHistory_id(){return history_id;}
	public String getUser_id(){return user_id;}
	public String getStatus(){return status;}
		
	
}
