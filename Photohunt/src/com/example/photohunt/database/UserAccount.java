package com.example.photohunt.database;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

public class UserAccount {
	public String TAG = "UserAccount";
	
	private String user_id;
	private String fb_name;
	private String password;

	private int found_one_lv;
	private int found_two_lv;
	private int found_three_lv;
	

	
	 public UserAccount(String user_id_in, String fb_name_in,String password_in,int found_one_lv_in,int found_two_lv_in,int found_three_lv_in) {
		 
		 this.user_id = user_id_in;
		 this.fb_name = fb_name_in;
		 this.password = password_in;
		 this.found_one_lv = found_one_lv_in;
		 this.found_two_lv = found_two_lv_in;
		 this.found_three_lv = found_three_lv_in;
		
		 
		 
	 }
	 public String getFacebookID() { return user_id; }
	 public String getFacebookName(){return fb_name;}
	 public String getPassword() {return password;}
	 public int getFoundLvOne(){return found_one_lv;}
	 public int getFoundLvTwo(){return found_two_lv;}
	 public int getFoundLvThree(){return found_three_lv;}
	 

	
	
	
	
}