package com.example.photohunt.database;


import java.util.ArrayList;
import java.util.Collections;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class Database extends SQLiteOpenHelper 

{
	// All Static variables
    // Database Version
	public static String TAG= "Database_Photohunt";
    private static final int DATABASE_VERSION = 1;
 
    // Database Name
    private static final String DATABASE_NAME = "database";
 
    // Contacts table name
    private static final String TABLE_PHOTO = "photo";
    private static final String TABLE_USER = "user_account";
    private static final String TABLE_HISTORY = "history";
    private ArrayList<Photo> allPhoto_list = new ArrayList<Photo>();
    private ArrayList<Photo> playingQueue = new ArrayList<Photo>(); 
    private ArrayList<Photo> successPhoto_list = new ArrayList<Photo>();
    private UserAccount currentUser ;
    private Photo temp_photo ;
	public Database(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		
		
		// Create user_account TABLE 
	    db.execSQL("CREATE TABLE " + TABLE_USER + 
		          "(user_id TEXT(100) PRIMARY KEY," +
		          " fb_name TEXT(100)," +
		          " password TEXT(100)," +
		          " found_lv_one INTEGER," +
		          " found_lv_two INTEGER," +
		          "	found_lv_three INTEGER);");
	   
	    Log.d(TAG,"Create Table User Account Successfully.");
	    ////////////////////////////////////////////////////////////////
	     
	    try{
	    	
	    // Craete photo TABLE 
	    db.execSQL("CREATE TABLE " + TABLE_PHOTO + 
		          "(photo_id INTEGER PRIMARY KEY AUTOINCREMENT," +
		          " longtitude DOUBLE," +
		          " latitude DOUBLE," +
		          " rating INT,"+
		          " descript TEXT(100)," +
		          "	path TEXT(100));");
	    }catch(Exception x){
	    	Log.d(TAG,""+x);
	    	}
	    
	    Log.d(TAG,"Create Table Photo Successfully.");
	    

	    ////////////////////////////////////////////////////////////////
	    try{
	    	// Create history TABLE
	    db.execSQL("CREATE TABLE " + TABLE_HISTORY + 
		          "(history_id INTEGER PRIMARY KEY AUTOINCREMENT," +
		          " user_id	 TEXT(100)," +
		          " photo_id INT," +
		          " status TEXT(100));");
	    }catch(Exception x){
	    	Log.d(TAG,""+x);
	    }        
	  
	    Log.d(TAG,"Create Table History Successfully.");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}
	
	private static Database instance; // 

	public static synchronized Database getHelper(Context context) { // Singleton Model for unique Database
		if (instance == null)
			instance = new Database(context);
		return instance;
	}
	
	// Insert User_account to user_account Table
	public long InsertUser(String user_id_in,String fb_name_in, String password_in, int lv_one,int lv_two,int lv_three ) {
		// TODO Auto-generated method stub
		
		 try {
			SQLiteDatabase db;
	    		db = this.getWritableDatabase(); // Write Data

	    	   ContentValues Val = new ContentValues();
	    	   Val.put("user_id", user_id_in); 
	    	   Val.put("fb_name", fb_name_in);
	    	   Val.put("password", password_in);
	    	   Val.put("found_lv_one",lv_one);
	    	   Val.put("found_lv_two", lv_two);
	    	   Val.put("found_lv_three",lv_three);
	    	   
			long rows = db.insert(TABLE_USER, null, Val);

			db.close();
			return rows; // return rows inserted.
	          
		 } catch (Exception e) {
		    Log.d(TAG,"Error Occer at Insert User:"+e);
		    return -1;
		 }

	}
	
	// Insert a Photo to Photo Table 
	public long InsertPhoto( double latitude_in, double longtitude_in,int rating_in,String descript_in,String path_in ) {
		// TODO Auto-generated method stub
		
		 try {
			SQLiteDatabase db;
	    		db = this.getWritableDatabase(); // Write Data
	    					
	    	   ContentValues Val = new ContentValues();
	    	 //  Val.put("photo_id", photo_id_in); 
	    	   Val.put("latitude", latitude_in);
	    	   Val.put("longtitude",longtitude_in);
	    	   Val.put("rating", rating_in);
	    	   Val.put("descript", descript_in);
	    	   Val.put("path",path_in);
	    	
	    	   
			long rows = db.insert(TABLE_PHOTO, null, Val);
			
			Log.d(TAG,"@ longtitude_in+"+longtitude_in);
			Log.d(TAG,"@ latitude_in+"+latitude_in);
			

			db.close();
			return rows; // return rows inserted.
	          
		 } catch (Exception e) {
		    return -1;
		 }

	}
	// Insert a History to History Table 
	public long InsertHistory(String user_id_in,int photo_id_in,String status_in) {
		// TODO Auto-generated method stub
		
		 try {
			SQLiteDatabase db;
	    		db = this.getWritableDatabase(); // Write Data
	    					
	    	   ContentValues Val = new ContentValues();
	    	   Val.put("user_id", user_id_in);
	    	   Val.put("photo_id",photo_id_in);
	    	   Val.put("status",status_in);
	  
			long rows = db.insert(TABLE_HISTORY, null, Val);

			db.close();
			return rows; // return rows inserted.
	          
		 } catch (Exception e) {
		    return -1;
		 }

	}
	
	public String  CheckExistingUserAccount(String user_id_in)
	{
		try{
			SQLiteDatabase db;
			db = this.getReadableDatabase();
			Cursor mCursor = db.rawQuery("SELECT * FROM " + TABLE_USER + " WHERE  user_id =?", new String[]{user_id_in});
			
			if(mCursor.getCount()>0)
			{
				return "exist";
			}
			else
			{	return "not_exist";
				
			}
		
		}catch(Exception x)
		{
			Log.d(TAG,"Error at CheckUserAccount"+x);
			return "Error occer";
		}
		
		
	}
	
	// call this function for checking input User   
	public String AuthenUserAccount (String user_id_in,String password_in)
	{
		try{
			SQLiteDatabase db;
			db = this.getReadableDatabase();
			String selectQuery  = " SELECT *" +
								  "	FROM user_account u " +
								  " WHERE u.user_id='"+user_id_in+"' AND u.password = '"+password_in+"'";
			Cursor mCursor = db.rawQuery(selectQuery,null);
			
			
			if(mCursor.getCount()>0)
			{
				return "success";
			}
			else
			{	return "failed";
				
			}
			
			
		}catch(Exception x)
		{
			Log.d(TAG,"Error at AuthenUserAccount"+x);
			return "Error occer";
		}
		
		
	}
	
	
	// get all photos from photo table and add to it  allPhoto Arraylist  
	 public ArrayList<Photo> getAllPhoto() {
	        try {
	            allPhoto_list.clear();
	            // Select All Query
	            String selectQuery = "SELECT * FROM " + TABLE_PHOTO;	         
	            SQLiteDatabase db = this.getWritableDatabase();	           
	            Cursor cursor = db.rawQuery(selectQuery, null);
	            // looping through all rows and adding to list
	            if (cursor.moveToFirst()) {
	                do {
	                	 Double temp_longitude = cursor.getDouble(1); // Bug Fixed
						 Double temp_latitude = cursor.getDouble(2); // 
	                     Photo photo = new Photo(
	                    		cursor.getInt(0),
	                            temp_longitude.toString(),
	                            temp_latitude.toString(),
	                            cursor.getInt(3),
	                            cursor.getString(4),
	                            cursor.getString(5)
	                    );
	                    allPhoto_list.add(photo);
	                } while (cursor.moveToNext());
	            }
	            cursor.close();
	            db.close();
	            Collections.reverse(allPhoto_list);
	            return allPhoto_list;
	        } catch (Exception e) {
	            // TODO: handle exception
	            Log.e(TAG, "Error at getAllPhoto" + e);
	        }
	        return allPhoto_list;
	    }
	 
	 	// get success photo from History Table and Photo Table 
	 public ArrayList<Photo> getSuccessPhoto (String user_id_in)
	 {
		 successPhoto_list.clear();
		 try{
			 SQLiteDatabase db = this.getReadableDatabase();
			 String selectQuery = " SELECT P.photo_id,P.longtitude,P.latitude,P.rating,P.descript,P.path"+
					 			  " FROM photo P ,history H "+
					 			  " WHERE P.photo_id = H.photo_id AND H.user_id="+"'"+ user_id_in +"'"+" AND "+"H.status='success'";
			 
			 Log.d(TAG,"Before selectQuery");
			 Cursor cursor =  db.rawQuery(selectQuery, null);
			
			 Log.d(TAG,"after selectQuery");
			 if(cursor.moveToFirst()){
				 
				 Double temp_longitude = cursor.getDouble(1);
				 Double temp_latitude = cursor.getDouble(2);
				 do{
					 Photo photo = new Photo(
					    		cursor.getInt(0),
	                            temp_longitude.toString(),
	                            temp_latitude.toString(),
	                            cursor.getInt(3),
	                            cursor.getString(4),
	                            cursor.getString(5)				 
							 );
					 
					 Log.d(TAG,""+cursor.getInt(0));
					 Log.d(TAG,""+cursor.getString(1));
					 Log.d(TAG,""+cursor.getString(2));
					 Log.d(TAG,""+cursor.getInt(3));
					 Log.d(TAG,""+cursor.getString(4));
					 Log.d(TAG,""+cursor.getString(5));
					 successPhoto_list.add(photo);
					
					
				 }while(cursor.moveToNext());
				 
			 }
			 cursor.close();
			 db.close();
			 Collections.reverse(successPhoto_list);
			 return successPhoto_list;
			  
		 }catch(Exception x){
			 Log.d(TAG,"Error at GetSuccessPhoto: "+x);
		 }
		 	return successPhoto_list;
		 
	 }
	 // get finding photo from History table and photo table
	 public ArrayList<Photo> getPlayingQueue(String user_id_in){
		 try{
			 
			 playingQueue.clear();
			 SQLiteDatabase db = this.getReadableDatabase();
			 String selectQuery = " SELECT P.photo_id,P.longtitude,P.latitude,P.rating,P.descript,P.path"+
					 			  " FROM photo P ,history H "+
					 			  " WHERE P.photo_id = H.photo_id AND H.user_id="+"'"+ user_id_in +"'"+" AND "+"H.status='finding'";
			 
			 Log.d(TAG,"Before selectQuery");
			 Cursor cursor =  db.rawQuery(selectQuery, null);
			
			 Log.d(TAG,"after selectQuery");
			 if(cursor.moveToFirst()){
				 
				 Double temp_longitude = cursor.getDouble(1);
				 Double temp_latitude = cursor.getDouble(2);
				 do{
					 Photo photo = new Photo(
					    		cursor.getInt(0),
	                            temp_longitude.toString(),
	                            temp_latitude.toString(),
	                            cursor.getInt(3),
	                            cursor.getString(4),
	                            cursor.getString(5)				 
							 );
					 
					 Log.d(TAG,""+cursor.getInt(0));
					 Log.d(TAG,""+cursor.getString(1));
					 Log.d(TAG,""+cursor.getString(2));
					 Log.d(TAG,""+cursor.getInt(3));
					 Log.d(TAG,""+cursor.getString(4));
					 Log.d(TAG,""+cursor.getString(5));
					 playingQueue.add(photo);
					
					
				 }while(cursor.moveToNext());
				 
			 }
			 cursor.close();
			 db.close();
			 
	//		 ArrayList<Photo> playingQueueRevert = new ArrayList<Photo>(); 
			 
			 Collections.reverse(playingQueue); // reorder photo in arraylist for descending List
			 
			 return   playingQueue;
			 
		 }catch(Exception x){
			 Log.d(TAG,"Error GetPlayingQueue: "+x);
		 }
		 
		 return playingQueue;
	 }
	 
	 //get user account form user_id input
	 public UserAccount getUserAccount (String user_id_in)
	{
		 try{
			 SQLiteDatabase db = this.getReadableDatabase();
			 String select_user = " SELECT * "+
					 			  " FROM user_account u"+
					 			  " WHERE u.user_id = "+"'"+user_id_in+"'";
			 Cursor cursor =  db.rawQuery(select_user, null);
			 if(cursor.moveToFirst()){
				 
				 do{
					 currentUser = new UserAccount(
							 	cursor.getString(0),
							 	cursor.getString(1),
							 	cursor.getString(2),
							 	cursor.getInt(3),
							 	cursor.getInt(4),
							 	cursor.getInt(5)
							 );
					
				 }while(cursor.moveToNext()); 
			 }
			 cursor.close();
			 db.close();
			return currentUser;
			
		 }catch(Exception x)
		 {
			Log.d(TAG,"Error occer at getUserAccunt : "+x ); 
		 }
		 
		 return currentUser;
	 }	
	 
	 // get Photo from photo_id input
	 public Photo getPhoto (String photo_id_in)
	 {
		 Log.d(TAG,"Start getPhoto");
		
		 try{
			 SQLiteDatabase db = this.getReadableDatabase();
			 String select_user = " SELECT * "+
					 			  " FROM  photo p"+
					 			  " WHERE p.photo_id = "+"'"+photo_id_in+"'";
			 Cursor cursor =  db.rawQuery(select_user, null);
			 if(cursor.moveToFirst()){
				 
				 do{
					 Double temp_longitude = cursor.getDouble(1);
					 Double temp_latitude = cursor.getDouble(2);
					 
					 
					 	temp_photo = new Photo(
					    		cursor.getInt(0),
	                            temp_longitude.toString(),
	                            temp_latitude.toString(),
	                            cursor.getInt(3),
	                            cursor.getString(4),
	                            cursor.getString(5)		
					 				);
					
				 }while(cursor.moveToNext()); 
			 }
	
	          
			 
			 cursor.close();
			 db.close();
			return temp_photo;
			
		 }catch(Exception x)
		 {
			Log.d(TAG,"Error occer at getPhoto : "+x ); 
		 }
		 
		 	return temp_photo;
		 
		 
	 }
	 
	 // Check in case of user try to add same photo to playing queue
	 public String CheckExistingHistory (String user_id_in,String photo_id_in)
	 {
			try{
				SQLiteDatabase db;
				db = this.getReadableDatabase();
				String selectQuery  = " SELECT *" +
									  "	FROM history h" +
									  " WHERE h.user_id='"+user_id_in+"' AND h.photo_id = '"+photo_id_in+"'";
				Cursor mCursor = db.rawQuery(selectQuery,null);
				
				
				if(mCursor.getCount()>0)
				{
					return "exist";
				}
				else
				{	return "not exist";
					
				}
				
				
				
			}catch(Exception x)
			{
				Log.d(TAG,"Error at CheckUserAccount"+x);
				return "Error occer";
			}
		 
		 
	 }
	 
	 // call this function when user success to find photo 
	 public String UpdateUserFinished(String user_id_in,int photo_id_in,int new_found_value, int photo_lv_in){
		 
		 String PHOTO_LV = "";
		 
		 try{
			 if(photo_lv_in==1)
			 {
				 PHOTO_LV = "found_lv_one";
				 
			 }else if(photo_lv_in==2)
			 {
				 PHOTO_LV = "found_lv_two";
				 
			 }else if (photo_lv_in ==3)
			 {
				 PHOTO_LV = "found_lv_three";
				 
			 }else{
				 Log.d(TAG,"Error occer at EditUserFinished: @ photo_lv_in");
			 }
			 
					SQLiteDatabase db;
			    	db = this.getWritableDatabase(); // Write Data
			    	String updateUserAccountQuery = " UPDATE user_account "+
			    						 " SET "+ PHOTO_LV +" ="+ "'"+ new_found_value +"'"+
			    						 " WHERE "+"user_id "+"="+ "'" + user_id_in + "'";
			    			
			    	db.execSQL(updateUserAccountQuery);
			    	
			    	String updateHistory = " UPDATE history "+
			    						   " SET "+"status="+"'success'" +
			    						   " WHERE "+"user_id ="+"'"+user_id_in+"'"+" AND " +"photo_id ='"+photo_id_in +"'";
			    	db.execSQL(updateHistory);
			    	
			    			
			 
		 }catch(Exception x)
		 {
			 Log.d(TAG,"Error occer at EditUserFinished: "+x);
			 return "failed";
		 }
		 
		 
		 return "success";
	 }
	 
	 
	
	
} 